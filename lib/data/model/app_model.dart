// import 'package:dashboard_app/data/model/type_app.dart';

class AppModel {
  final int id;
  final String appName;
  // final String publisher;
  // final apptype type;
  // final String age;
  // final String totalReview;
  // final String intro;
  // final String policy;
  // final String rating;
  AppModel(this.id, this.appName);
  Map<String, dynamic> toJson(){
    var map = <String , dynamic> {
      'App_id': id,
      'App_name': appName,
    };
    return map;
  }

}
