class GioHangModel {
  final int? id;
  final String? nameSanPham;
  final String? urlSanPham;
  final String? giaSanPham;
  GioHangModel({this.id, this.nameSanPham, this.giaSanPham, this.urlSanPham});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'UserName': nameSanPham,
      'urlSanPham': urlSanPham,
      'giaSanPham': giaSanPham,
    };
    return map;
  }
  // @override
  // List<Object> get props => [id,nameSanPham,urlSanPham,giaSanPham];
}
