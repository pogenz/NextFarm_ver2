final Map<String, String> imagesSrc = {
  "áo đồng phục": "assets/images/ao_dong_phuc1.jpg",
  "áo đồng phục nữ": "assets/images/ao_dong_phuc_nu.jpg",
  "áo golf nam": "assets/images/ao_golf_nam.jpeg",
  "áo golf nữ": "assets/images/ao_golf_nu.jpeg",
  "phụ kiện thời trang": "assets/images/phu_kien_thoi_trang.jpeg",
  "polo nam": "assets/images/polo_nam.jpeg",
  "quần golf nam": "assets/images/quan_golf_nam.jpeg",
  "quần golf nữ ": "assets/images/quan_golf_nu.jpeg",
  "váy ngắn": "assets/images/vay_ngan.jpeg",
  "váy dài": "assets/images/vay_dai.jpeg",
  "gậy golf": "assets/images/gay_golf.jpeg",
  "giày thể thao": "assets/images/giay_the_thao.jpeg",
};

// title sản phẩm của shop
final List<String> imagesTitle = [
  "áo đồng phục",
  "áo đồng phục nữ",
  "áo golf nam",
  "áo golf nữ",
  "phụ kiện thời trang",
  "polo nam",
  "quần golf nam",
  "quần golf nữ ",
  "váy ngắn",
  "váy dài",
  "gậy golf",
  "giày thể thao",
];

class CardSanPhamModel {
  final int? id;
  // final String? username;
  // final String? password;
  // final String? email;
  final String? imgSrc;
  final String? imgTitle;
  CardSanPhamModel({
    this.id,
    this.imgSrc,
    this.imgTitle,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{"ImgSrc": imgSrc, "ImgTitle": imgTitle};
    return map;
  }
  // @override
  // List<Object> get props => [id,username,password,email];
}
