class UserModel {
  final int? id;
  final String? username;
  final String? password;
  final String? email;
  final String? userImg;
  UserModel({this.id, this.username, this.email, this.password, this.userImg});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'UserImg': userImg,
      'UserName': username,
      'password': password,
      'email': email,
    };
    return map;
  }
  // @override
  // List<Object> get props => [id,username,password,email];
}
