import 'package:flutter/material.dart';
import 'package:next_farm_ver2/ui/screen/homepage/homepage.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';
import 'package:next_farm_ver2/data/model/user_model.dart';

class User extends StatefulWidget {
  User(
    this.userModel, {
    super.key,
  });
  UserModel userModel;
  @override
  State<User> createState() => _UserState();
}

class _UserState extends State<User> {
  Widget _buildBottomSheet(BuildContext context) {
    return BottomSheet(
      onClosing: () {},
      builder: (context) {
        return Container(
          height: 300,
          child: Center(
            child: Text('Đây là BottomSheet'),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return BaseScreen(
      hideAppBar: true,
      body: Container(
        height: size.height,
        width: size.width,
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Container(
              height: 200,
              width: 200,
              // decoration: BoxDecoration(
              //   borderRadius: BorderRadius.all(Radius.circular(40.0)),
              // ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: Image(
                  image: AssetImage(widget.userModel.userImg ?? ""),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Name : ${widget.userModel.username ?? ""}".toCapitalized(),
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Email : ${widget.userModel.email ?? ""}".toCapitalized(),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
            ),
            Container(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    TextButton(
                        onPressed: () {
                          showModalBottomSheet(
                            context: context,
                            builder: (BuildContext context) {
                              return _buildBottomSheet(context);
                            },
                          );
                        },
                        child: Text("sửa ")),
                    TextButton(onPressed: () {}, child: Text("more ")),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
