import 'package:flutter/material.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';
import 'package:next_farm_ver2/ui/screen/shop/shop.dart';
import 'package:next_farm_ver2/ui/screen/tintuc/tintuc.dart';
import 'package:next_farm_ver2/ui/screen/taikhoan/taikhoan.dart';
import 'package:next_farm_ver2/ui/screen/homepage/homepage.dart';

class MyAppNextFarm extends StatefulWidget {
  final int? indexScreen;
  MyAppNextFarm({super.key, this.indexScreen});

  @override
  State<MyAppNextFarm> createState() => _MyAppNextFarmState();
}

class _MyAppNextFarmState extends State<MyAppNextFarm> {
  late int _currentIndex = widget.indexScreen ?? 0;
  String _appbarTitle = "Home Page";

  static List<String> _appbarTitles = [
    "Home Page",
    "Tin Tuc",
    "Shop",
    "Tai Khoan"
  ];
  final List<Widget> _children = [
    HomePage(),
    TinTuc(),
    ShopNextVision(),
    TaiKhoan(),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      _appbarTitle = _appbarTitles[index];
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      hideAppBar: true,
      hideNavigationBar: false,
      backgroundcolor: Colors.white,
      // customAppBar: AppBar(
      //   title: Text(_appbarTitle),
      //   centerTitle: true,
      //   backgroundColor: ,
      // ),
      body: IndexedStack(
        index: _currentIndex,
        children: _children,
      ),
      customBottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.document_scanner),
            label: 'Tin tuc',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart_sharp),
            label: 'Shop',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Tai khoan',
          ),
        ],
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.orange[700],
        unselectedItemColor: Colors.grey,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        onTap: onTabTapped,
      ),
    );
  }
}
