import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:next_farm_ver2/res/images.dart';
import 'package:next_farm_ver2/ui/screen/auth/login_screen_new.dart';
import 'package:next_farm_ver2/ui/screen/cau_hinh_tuoi/custom_cau_hinh_tuoi.dart';
import 'package:next_farm_ver2/ui/screen/homepage/homepage.dart';
import 'package:next_farm_ver2/ui/screen/screen.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';
import 'package:next_farm_ver2/ui/widget/widget.dart';
import 'package:scale_size/scale_size.dart';

import '../myappNextfarm/myappNextfarm.dart';

class WateringScreen extends StatefulWidget {
  const WateringScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<WateringScreen> createState() => _WateringScreenState();
}

class _WateringScreenState extends State<WateringScreen> {
  List<String> listarea = <String>[
    'Hà Nội',
    'Sài Gòn',
    'Hải Dương',
    'Hoà bình'
  ];
  List<String> liststage = <String>[
    'Giai đoạn 1',
    'Giai đoạn 2',
    'Giai đoạn 3',
    'Giai đoạn 4'
  ];
  List<String> listwater = <String>[
    '100 lít',
    '200 lít',
    '300 lít',
    '400 lít',
    '500 lít'
  ];
  List<String> listpoop = <String>['1 lít', '2 lít', '3 lít', '4 lít', '5 lít'];
  String? dropdownvalue;
  String? dropdownValueArea;
  String? dropdownValueStage;
  String? dropdownValuewater;
  String? dropdownValuepoop1;
  String? dropdownValuepoop2;
  String? dropdownValuepoop3;
  String? dropdownValuepoop4;
  String? dropdownValuepoop5;
  String? dropdownValuepoop6;
  String? dropdownValuepoop7;
  String? dropdownValuepoop8;
  String? dropdownValuepoop9;
  String? dropdownValuepoop10;
  String? dropdownValuepoop11;
  String? dropdownValuepoop12;

  List<bool> _checkedList = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  List<String> listtub = <String>[
    'Bồn 1',
    'Bồn 2',
    'Bồn 3',
    'Bồn 4',
    'Bồn 5',
    'Bồn 6',
    'Bồn 7',
    'Bồn 8',
    'Bồn 9',
    'Bồn 10',
    'Bồn 11',
    'Bồn 12',
  ];

  List<String> WatertimeText = [
    'Thời gian tưới lần 1',
    'Thời gian tưới lần 2',
    'Thời gian tưới lần 3',
    'Thời gian tưới lần 4',
    'Thời gian tưới lần 5',
    'Thời gian tưới lần 6',
    'Thời gian tưới lần 7',
    'Thời gian tưới lần 8',
    'Thời gian tưới lần 9',
    'Thời gian tưới lần 10',
    'Thời gian tưới lần 11',
    'Thời gian tưới lần 12',
  ];
  List<String> timeValue = [
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
    '6 giờ',
  ];

  List<String> Reday = [
    'Thứ 2',
    'Thứ 3',
    'Thứ 4',
    'Thứ 5',
    'Thứ 6',
    'Thứ 7',
    'Chủ Nhật',
  ];

  DateTime? selectedDate;

  List<DropdownMenuItem<DateTime>> _buildDropdownItems() {
    final List<DropdownMenuItem<DateTime>> items = [];
    for (int i = 1; i < 365; i++) {
      final date = DateTime.now().add(Duration(days: i));
      if (!items.any((item) => item.value == date)) {
        final item = DropdownMenuItem<DateTime>(
          value: date,
          child: Text(
            DateFormat('dd/MM/yyyy').format(date),
          ),
        );
        items.add(item);
      }
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return BaseScreen(
      // customAppBar: AppBar(
      //   // titleSpacing: 10,
      //   // elevation: 0,
      //   backgroundColor: Colors.green,
      //   automaticallyImplyLeading: false,
      //   actions: [
      //     Row(
      //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //       children: [
      //         // quyet fix
      //         // Spacer(),
      //         SizedBox(
      //           width: size.width - 50,
      //         ),
      //         IconButton(
      //             onPressed: () {
      //               Navigator.pop(context);
      //             },
      //             icon: Icon(Icons.arrow_back_ios)),
      //         // IconButton(
      //         //   icon: Icon(Icons.home),
      //         //   color: Colors.white,
      //         //   onPressed: () {
      //         //     Navigator.push(context,
      //         //         MaterialPageRoute(builder: (context) => MyAppNextFarm()));
      //         //   },
      //         // ),
      //       ],
      //     ),
      //   ],
      // ),
      // hideAppBar: true,
      // quyet fix
      customAppBar: AppBar(
        backgroundColor: Colors.green,
      ),
      backgroundcolor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: SizedBox(
          child: ListView(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Center(
                  child: Text(
                    "Cấu hình tưới",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                ),
              ),
              SizedBox(),
              Text(
                "1. Khu vực",
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.green.withOpacity(0.15),
                ),
                height: 70,
                width: 200,
                child: Row(
                  children: [
                    Container(
                      height: 100,
                      width: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Image.asset(
                        'assets/images/logo_nextfarm2.png',
                        fit: BoxFit.contain,
                      ),
                      margin: EdgeInsets.all(5),
                    ),
                    SizedBox(width: 0),
                    Text(
                      'Hãy chọn khu vực tưới',
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 14,
              ),
              //chọn khu vực
              DropdownbuttonStageArea(
                  listItems: listarea,
                  labelText: "Chọn khu vực",
                  onChanged: (String? valueArea) {
                    setState(() {
                      dropdownValueArea = valueArea!;
                    });
                  },
                  dropdownValue:
                      dropdownValueArea != null ? dropdownValueArea : null),
              SizedBox(
                height: 10,
              ),
              Text(
                "2. Giai đoạn tưới",
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 10),
              //giai đoạn tưới
              DropdownbuttonStageArea(
                  listItems: liststage,
                  labelText: "Chọn giai đoạn",
                  onChanged: (String? valueStage) {
                    setState(() {
                      dropdownValueStage = valueStage!;
                    });
                  },
                  dropdownValue:
                      dropdownValueStage != null ? dropdownValueStage : null),
              SizedBox(height: 10),
              //Ngày bắt đầu
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Row(
                  children: [
                    Text("Ngày bắt đầu: ", style: TextStyle(fontSize: 16)),
                    Padding(padding: EdgeInsets.only(right: 20)),
                    Expanded(
                      child: CustomContainer(
                        height: 45,
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: Icon(Icons.calendar_today_outlined,
                                  color: Colors.grey),
                            ),
                            Expanded(
                              child: DropdownButton(
                                icon: Icon(Icons.arrow_right_sharp),
                                underline: Container(),
                                hint: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Hôm nay, T6 (18/3)',
                                      style: TextStyle()),
                                ),
                                value: dropdownValuepoop4 != null
                                    ? dropdownValuepoop4
                                    : null,
                                isExpanded: true,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                onChanged: (String? valueStage) {
                                  setState(() {
                                    dropdownValuepoop4 = valueStage!;
                                  });
                                },
                                borderRadius: BorderRadius.circular(10),
                                items: listpoop.map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(value),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Row(
                  children: [
                    Text("Ngày kết thúc: ", style: TextStyle(fontSize: 16)),
                    Padding(padding: EdgeInsets.only(right: 20)),
                    Expanded(
                      child: CustomContainer(
                        height: 45,
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: Icon(Icons.calendar_today_outlined,
                                  color: Colors.grey),
                            ),
                            Expanded(
                              child: DropdownButton(
                                icon: Icon(Icons.arrow_right_sharp),
                                underline: Container(),
                                hint: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Hôm nay, T6 (18/3)',
                                      style: TextStyle()),
                                ),
                                value: dropdownValueStage != null
                                    ? dropdownValueStage
                                    : null,
                                isExpanded: true,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                onChanged: (String? valueStage) {
                                  setState(() {
                                    dropdownValueStage = valueStage!;
                                  });
                                },
                                borderRadius: BorderRadius.circular(10),
                                items: liststage.map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(value),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 10,
              ),
              Text(
                "3. Chương trình tưới",
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 10),
              Center(
                child: Text(
                  "Cấu hình nước tưới",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Row(
                  children: [
                    Expanded(
                      child: CustomContainer(
                        height: 45,
                        child: Row(
                          children: [
                            Expanded(
                              child: DropdownButton(
                                underline: Container(),
                                hint: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Lượng nước',
                                      style: TextStyle(color: Colors.black)),
                                ),
                                value: dropdownvalue != null
                                    ? dropdownvalue
                                    : null,
                                isExpanded: true,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                onChanged: (String? valuewater) {
                                  setState(() {
                                    dropdownValuewater = valuewater!;
                                  });
                                },
                                borderRadius: BorderRadius.circular(10),
                                items: listwater.map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(value),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    CustomContainer(
                      height: 45,
                      width: MediaQuery.of(context).size.width * 0.47,
                      child: Center(
                        child: Text(
                          dropdownValuewater ?? 'No item selected',
                          style: TextStyle(fontSize: 14.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Center(
                child: Text(
                  "Cấu hình châm phân",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              SizedBox(height: 10),
              ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 12,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: 50,
                              child: Text(
                                listtub[index],
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(right: 10)),
                            Expanded(
                              child: CustomContainer(
                                height: 45,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: DropdownButton(
                                          icon: Icon(Icons.arrow_right_sharp),
                                          underline: Container(),
                                          hint: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text('Lượng phân',
                                                style: TextStyle()),
                                          ),
                                          value: dropdownvalue != null
                                              ? dropdownvalue
                                              : null,
                                          isExpanded: true,
                                          elevation: 16,
                                          style: const TextStyle(
                                              color: Colors.black),
                                          onChanged: (String? valuepoop) {
                                            setState(() {
                                              dropdownValuepoop1 = valuepoop!;
                                            });
                                          },
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          items: listpoop
                                              .map<DropdownMenuItem<String>>(
                                                  (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Text(value),
                                              ),
                                            );
                                          }).toList(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(width: 8),
                            CustomContainer(
                              height: 45,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Center(
                                child: Text(
                                  dropdownValuepoop1 ?? 'No item selected',
                                  style: TextStyle(fontSize: 14.0),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                      ],
                    ),
                  );
                },
              ),
              SizedBox(height: 10),
              ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 12,
                itemBuilder: (BuildContext context, int index) {
                  String currentWaterTimeText = WatertimeText[index];
                  String currentTimeValue = timeValue[index];
                  return Padding(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: CustomContainer(
                                height: 45,
                                child: Center(
                                  child: Text(currentWaterTimeText),
                                ),
                              ),
                            ),
                            SizedBox(width: 8),
                            CustomContainer(
                              height: 45,
                              width: 100,
                              child: Center(
                                child: Text(currentTimeValue),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                      ],
                    ),
                  );
                },
              ),
              SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.white12,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.6),
                        blurRadius: 0.1,
                        offset: const Offset(0, 0.1),
                      ),
                    ],
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Center(
                    child: Text("Lặp lại theo thứ",
                        style: TextStyle(fontSize: 16)),
                  ),
                ),
              ),
              SizedBox(height: 10),
              //Lặp lại theo thứ
              ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: 7,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: CustomContainer(
                                height: 45,
                                width: 100,
                                child: Center(
                                  child: Text(Reday[index] ?? ""),
                                ),
                              ),
                            ),
                            SizedBox(width: 8),
                            CustomContainer(
                              height: 45,
                              width: 100,
                              child: Checkbox(
                                value: _checkedList[index],
                                onChanged: (bool? value) {
                                  setState(() {
                                    _checkedList[index] = value!;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8)
                      ],
                    ),
                  );
                },
              ),
              SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Expanded(
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.white12,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.6),
                          blurRadius: 0.1,
                          offset: const Offset(0, 0.1),
                        ),
                      ],
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: Center(
                      child: Text("Lặp lại theo ngày",
                          style: TextStyle(fontSize: 16)),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Row(
                  children: [
                    Expanded(
                      child: CustomContainer(
                        height: 45,
                        child: Center(
                          child: Text("Lặp lại sau"),
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    CustomContainer(
                      height: 45,
                      width: 100,
                      child: Center(
                        child: Text("6 ngày"),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              BaseButton(
                decoration: BoxDecoration(
                  color: Colors.white12,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      blurRadius: 0.1,
                      offset: const Offset(0, 0.1),
                    ),
                  ],
                  borderRadius: BorderRadius.circular(6),
                ),
                onTap: () {},
                child: Center(
                  child: Text(
                    "Hoàn tất",
                    style: TextStyle(color: Colors.black, fontSize: 20),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
