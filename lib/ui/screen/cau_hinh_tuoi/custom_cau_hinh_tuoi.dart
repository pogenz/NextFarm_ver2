import 'package:flutter/material.dart';



class CustomContainer extends StatelessWidget {
  final Widget child;
  final double? height;
  final double? width;
  const CustomContainer({
    Key? key,
    required this.child,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: Colors.white12,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            blurRadius: 0.1,
            offset: const Offset(0, 0.1),
          ),
        ],
        borderRadius: BorderRadius.circular(6),
      ),
      child: child,
    );
  }
}


class DropdownbuttonStageArea extends StatefulWidget {
  final List<String> listItems;
  final String labelText;
  final String? dropdownValue;
  final Function(String?) onChanged;

  const DropdownbuttonStageArea({
    Key? key,
    required this.listItems,
    required this.labelText,
    required this.onChanged,
    this.dropdownValue,
  }) : super(key: key);

  @override
  State<DropdownbuttonStageArea> createState() =>
      _DropdownbuttonStageAreaState();
}

class _DropdownbuttonStageAreaState extends State<DropdownbuttonStageArea> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: CustomContainer(
        height: 45,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Icon(Icons.home_outlined, color: Colors.grey),
            ),
            Expanded(
              child: DropdownButton(
                icon: Icon(Icons.arrow_right_sharp),
                underline: Container(),
                hint: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(widget.labelText, style: TextStyle()),
                ),
                value: widget.dropdownValue,
                isExpanded: true,
                elevation: 16,
                style: const TextStyle(color: Colors.black),
                onChanged: widget.onChanged,
                borderRadius: BorderRadius.circular(10),
                items: widget.listItems
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(value),
                    ),
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


