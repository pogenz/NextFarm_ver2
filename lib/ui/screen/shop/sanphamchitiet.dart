import 'package:flutter/material.dart';
import 'package:next_farm_ver2/ui/screen/homepage/homepage.dart';

class SanphamCard extends StatelessWidget {
  final VoidCallback? onPressed;
  final double? heightImg;
  final double? heightContainer;
  final double? widthContainer;
  final double? widthImg;
  final double? heightText;
  final double? heightSizedBox;
  final String? ImgSrc;
  final String? ImgTitle;
  final bool? isNewSanPham;
  final double? giaSanPham;
  const SanphamCard({
    super.key,
    this.heightImg,
    this.heightText,
    this.widthContainer,
    this.heightContainer,
    this.widthImg,
    this.heightSizedBox,
    this.onPressed,
    this.ImgSrc,
    this.ImgTitle,
    this.isNewSanPham,
    this.giaSanPham,
  });

  @override
  Widget build(BuildContext context) {
    // return Column(
    //   mainAxisAlignment: MainAxisAlignment.center,
    //   children: [
    //     InkWell(
    //       onTap: onPressed,
    //       child: Container(
    //         width: heightImg,
    //         height: widthImg,
    //         decoration: BoxDecoration(
    //             shape: BoxShape.circle,
    //             image: DecorationImage(
    //                 image: AssetImage(ImgSrc ?? ""), fit: BoxFit.cover)),
    //       ),
    //     ),
    //     SizedBox(
    //       height: heightSizedBox ?? 0,
    //     ),
    //     Text(
    //       ImgTitle ?? "",
    //       style: TextStyle(
    //           fontSize: heightText ?? 0.toDouble(),
    //           fontWeight: FontWeight.bold),
    //       overflow: TextOverflow.ellipsis,
    //     ),
    //     SizedBox(
    //       height: heightSizedBox ?? 0 * 2,
    //     ),
    //   ],
    // );
    return Container(
      height: heightContainer,
      width: widthContainer,
      decoration: BoxDecoration(color: Colors.white),
      child: InkWell(
        onTap: onPressed,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Stack(
              children: [
                Image.asset(
                  ImgSrc ?? "",
                  width: widthImg,
                  fit: BoxFit.cover,
                ),
                Positioned(
                  top: 5,
                  left: 5,
                  child: isNewSanPham == true
                      ? Container(
                          color: Color.fromARGB(255, 139, 17, 8),
                          child: Text(
                            "New",
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          ),
                        )
                      : Container(),
                ),
              ],
            ),
            SizedBox(
              height: 3,
            ),
            Text(
              "${ImgTitle} ".toCapitalized(),
              style: TextStyle(fontSize: 15, color: Colors.black),
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(
              height: 3,
            ),
            Text(
              "${giaSanPham} đ",
              style: TextStyle(
                  fontSize: 17,
                  color: Colors.orange,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }
}
