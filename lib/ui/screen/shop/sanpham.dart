import 'package:flutter/material.dart';
import 'package:next_farm_ver2/ui/screen/shop/sanphamchitiet-muahang.dart';
import 'dart:math';

import 'package:next_farm_ver2/ui/screen/shop/shop.dart';
import 'package:next_farm_ver2/ui/screen/shop/test.dart';
import '/data/model/card_san_pham_model.dart';

import 'package:next_farm_ver2/ui/widget/base_screen.dart';

import '../myappNextfarm/myappNextfarm.dart';
import '../shop/sanphamchitiet.dart';

class SanPhamShop extends StatefulWidget {
  final int? index;
  final String? indexTitle;
  final String? indexImg;
  bool isIndexThanhDieuHuong(int index) {
    if (index == 0) {
      return true;
    } else {
      return false;
    }
  }

  SanPhamShop({super.key, this.index, this.indexTitle, this.indexImg});

  @override
  State<SanPhamShop> createState() => _SanPhamShopState();
}

class _SanPhamShopState extends State<SanPhamShop> {
  // _SanPhamShopState(this.indexImg){
  //   _indexImg = widget.indexImg;
  // }
  // late String _ImgSrc =  widget.indexImg ?? "";
  int randomGiaSanPham(int min, int max) {
    return min + Random().nextInt(max - min);
  }

  late int giaSanPham = randomGiaSanPham(100, 500) * 100;

  late int SoSanPham = 10;
  double heightImg = 100.0;
  double widthImg = 100.0;
  String? _dropdownValue;
  // String? _selectedValue = "Mặc định".toCapitalized();
  List<String> dropdownValues = [
    "giá trị 1",
    "giá trị 2",
    "giá trị 3",
    "Mặc định",
  ];

  // final List<Widget> imageWidgets = List.generate(SoSanPham, Container()
  // (index) {
  //   return Container(
  //     // width: (size.width -10) *50
  //     child: (
  //       heightImg: heightImg,
  //       widthImg: widthImg,
  //       heightSizedBox: heightsizedBox,
  //       ImgTitle: imagesTitle[index].toCapitalized(),
  //       ImgSrc: imagesSrc[imagesTitle[index]],
  //       heightText: heightText,
  //       onPressed: () {
  //         Navigator.push(
  //             context,
  //             MaterialPageRoute(
  //                 builder: (context) => SanPhamShop(

  //                     )));
  //         //           // xử lý sự kiện khi ấn vào hình ảnh
  //       },
  //     ),
  //   );
  // },
  // );

  // test 2

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final List<Widget> imageWidgets = List.generate(
      10,
      (index) {
        return Container(
          decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
          width: (size.width) * .50,
          child: SanphamCard(
            widthImg: size.width * .48,
            // heightImg: 150,
            // heightContainer: 200,
            widthContainer: size.width * .50,
            isNewSanPham: index == 0 ? true : false,
            ImgSrc: widget.indexImg ?? "",
            ImgTitle: "${widget.indexTitle ?? ""} ${index + 1}",
            // ImgTitle: "ao nam  ${index + 1}",
            giaSanPham: giaSanPham.toDouble(),
            onPressed: () {
              print(index);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SanPhamChiTiet_MuaHang(
                            name: "${widget.indexTitle} ${index + 1}",
                            gia: giaSanPham.toDouble(),
                            imgSanPham: widget.indexImg,
                          )));
            },
          ),
        );
        // width: (size.width - 30) / 4,

        //           // xử lý sự kiện khi ấn vào hình ảnh
      },
    );
    int indexThanhDieuHuong = 0;
    @override

    // dropdownbutton tapbar

        final List<String> _ThanhDieuHuongText = ["tất cả ", "more"];
    final List<Widget> _ThanhDieuHuong =
        List.generate(_ThanhDieuHuongText.length, (index) {
      return Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: index == indexThanhDieuHuong
                        ? Colors.orange
                        : Colors.lightGreen,
                    width: 4))),
        // hoat dong ko tốt ko thể chỉnh theo
        child: TextButton(
            onPressed: () {},
            child: Text(
              "${_ThanhDieuHuongText[index]}".toUpperCase(),
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            )),
      );
    });
    return BaseScreen(
      backgroundcolor: Colors.white,
      hideAppBar: true,
      body: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10, right: 5),
            child: Container(
              height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyAppNextFarm(
                                          indexScreen: 2,
                                        )));
                          },
                          child: Icon(Icons.arrow_back_ios)),
                      SizedBox(
                        width: 15,
                      ),
                      Container(
                        width: (size.width - 15) * .50,
                        child: Text(
                          "${widget.indexTitle ?? ""}",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        height: 40,
                        width: 40,
                        child: Icon(Icons.search),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          color: Colors.grey[500],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 40,
                        width: 40,
                        child: Icon(Icons.more_vert),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          color: Colors.grey[500],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Column(
            children: [
              Container(
                width: size.width,
                color: Colors.lightGreen,
                child: Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: _ThanhDieuHuong,
                ),
              ),
            ],
          ),
          Container(
            height: 60,
            width: size.width,
            color: Colors.grey,
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                children: [
                  Container(
                    width: (size.width * 0.45),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // Text(
                        //   "mặc định".toCapitalized(),
                        //   style: TextStyle(fontSize: 15),
                        // ),

                        //test
                        DropdownButton(
                          hint: _dropdownValue == null
                              ? Text(
                                  "${_dropdownValue ?? "mặc định".toCapitalized()}")
                              : Text(
                                  _dropdownValue ?? "",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15),
                                ),
                          // isExpanded: true,
                          iconSize: 30.0,
                          style: TextStyle(color: Colors.black),
                          items: dropdownValues.map(
                            (val) {
                              return DropdownMenuItem<String>(
                                value: val,
                                child: Text(val),
                              );
                            },
                          ).toList(),
                          onChanged: (val) {
                            setState(
                              () {
                                _dropdownValue = val;
                              },
                            );
                          },
                        ),

                        // Icon(Icons.arrow_downward_sharp)
                      ],
                    ),
                  ),
                  Expanded(
                    // width: (size.width - 10) *,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Icon(Icons.filter_alt),
                        Text(
                          "Lọc".toCapitalized(),
                          style: TextStyle(fontSize: 15),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: size.width,
            height: size.height - 225,
            child: ListView(
              children: [
                Wrap(spacing: 0, runSpacing: 0, children: imageWidgets),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// viet hoa

// extension StringCasingExtension on String {
//   String toCapitalized() =>
//       length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';
//   String toTitleCase() => replaceAll(RegExp(' +'), ' ')
//       .split(' ')
//       .map((str) => str.toCapitalized())
//       .join(' ');
// }
// import StringCasingExtension

// final helloWorld = 'hello world'.toCapitalized(); // 'Hello world'
// final HelloWorld = 'hello world'.toUpperCase(); // 'HELLO WORLD'
// final helloWorldCap = 'hello world'.toTitleCase(); // 'Hello World'
