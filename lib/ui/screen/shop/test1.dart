import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:next_farm_ver2/data/model/user_model.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';

class test1Screen extends StatefulWidget {
  test1Screen(this.userModel, {super.key});

  UserModel userModel;

  @override
  State<test1Screen> createState() => _test1ScreenState();
}

class _test1ScreenState extends State<test1Screen> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      backgroundcolor: Colors.white,
      body: Container(child: Text(widget.userModel.email ?? "")),
    );
  }
}
