import 'package:flutter/material.dart';
import 'package:next_farm_ver2/ui/screen/homepage/homepage.dart';
import 'package:next_farm_ver2/ui/screen/shop/giohang.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';

class SanPhamChiTiet_MuaHang extends StatefulWidget {
  final String? name;
  final double? gia;
  final String? imgSanPham;
  const SanPhamChiTiet_MuaHang(
      {super.key, this.name, this.gia, this.imgSanPham});

  @override
  State<SanPhamChiTiet_MuaHang> createState() => _SanPhamChiTiet_MuaHangState();
}

class _SanPhamChiTiet_MuaHangState extends State<SanPhamChiTiet_MuaHang> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return BaseScreen(
      hideAppBar: true,
      backgroundcolor: Colors.white,
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.only(left: 5, right: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    // width: size.width * .50,
                    height: 80,
                    child: Row(
                      children: [
                        InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(Icons.arrow_back_ios)),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: (size.width - 10) * .50,
                          child: Text(
                            "${widget.name}".toCapitalized(),
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        height: 40,
                        width: 40,
                        child: Icon(Icons.search),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          color: Colors.grey[500],
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GioHangShop()));
                        },
                        child: Container(
                          height: 40,
                          width: 40,
                          child: Icon(Icons.shopping_cart_checkout),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            color: Colors.grey[500],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        height: 40,
                        width: 40,
                        child: Icon(Icons.more_vert),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          color: Colors.grey[500],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: ListView(
                  children: [
                    Container(
                        width: size.width * .90,
                        height: size.height * .50,
                        child: Image(
                          image: AssetImage(widget.imgSanPham ?? ""),
                          fit: BoxFit.cover,
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: Text(
                        "${widget.gia} d",
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 35,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: Text(
                        "${widget.name} ".toCapitalized(),
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 100,
              decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
              child: Padding(
                padding: EdgeInsets.only(left: 5, right: 0),
                child: Row(
                  children: [
                    InkWell(
                      onTap: () {
                        print("hoat dong tot");
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text("check mua san pham".toCapitalized()),
                            content: Text(
                                "Bạn có muốn thêm sản phẩm ${widget.name} vào giỏ hàng không ?"),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("Ok")),
                              TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("Cannel"))
                            ],
                          ),
                        );
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        width: (size.width - 10) * .50,

                        // height: 100,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(Icons.shopping_cart_rounded),
                            Text(
                              "thêm giỏ hàng".toUpperCase(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color.fromARGB(255, 165, 50, 41)),
                          // height: 100,
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "mua ngay".toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "không ưng đổi ngay".toCapitalized(),
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                )
                              ]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
