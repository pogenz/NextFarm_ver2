import 'package:flutter/material.dart';
import 'package:next_farm_ver2/data/model/user_model.dart';
import 'package:next_farm_ver2/ui/screen/drawScreen/curvePainterpage.dart';
import 'package:next_farm_ver2/ui/screen/shop/test1.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';
import 'package:next_farm_ver2/ui/screen/shop/test.dart';
import 'package:next_farm_ver2/ui/screen/shop/sanpham.dart';

class ShopNextVision extends StatefulWidget {
  const ShopNextVision({super.key});

  @override
  State<ShopNextVision> createState() => _ShopNextVisionState();
}

class _ShopNextVisionState extends State<ShopNextVision> {
  // ảnh sản phẩm của shop
  final Map<String, String> imagesSrc = {
    "áo đồng phục": "assets/images/ao_dong_phuc1.jpg",
    "áo đồng phục nữ": "assets/images/ao_dong_phuc_nu.jpg",
    "áo golf nam": "assets/images/ao_golf_nam.jpeg",
    "áo golf nữ": "assets/images/ao_golf_nu.jpeg",
    "phụ kiện thời trang": "assets/images/phu_kien_thoi_trang.jpeg",
    "polo nam": "assets/images/polo_nam.jpeg",
    "quần golf nam": "assets/images/quan_golf_nam.jpeg",
    "quần golf nữ ": "assets/images/quan_golf_nu.jpeg",
    "váy ngắn": "assets/images/vay_ngan.jpeg",
    "váy dài": "assets/images/vay_dai.jpeg",
    "gậy golf": "assets/images/gay_golf.jpeg",
    "giày thể thao": "assets/images/giay_the_thao.jpeg",
  };

  // title sản phẩm của shop
  final List<String> imagesTitle = [
    "áo đồng phục",
    "áo đồng phục nữ",
    "áo golf nam",
    "áo golf nữ",
    "phụ kiện thời trang",
    "polo nam",
    "quần golf nam",
    "quần golf nữ ",
    "váy ngắn",
    "váy dài",
    "gậy golf",
    "giày thể thao",
  ];

  // ảnh banner
  final List<String> images = [
    "assets/images/banner_shop1.jpeg",
    "assets/images/banner_shop2.jpg",
  ];
  List<UserModel> listUser = [
    UserModel(email: "zxczc"),
    UserModel(email: "zxczc"),
    UserModel(email: "zxczc"),
    UserModel(email: "zxczc"),
    UserModel(username: "abc")
  ];

  int _currentPage = 0;
  final _pageController = PageController();

  @override
  void initState() {
    super.initState();
    _pageController.addListener(() {
      setState(() {
        _currentPage = _pageController.page!.round();
      });
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double heightImg = 70.0;
    double widthImg = 70.0;
    double heightsizedBox = 10.0;
    double heightText = 20.0;

    final Size size = MediaQuery.of(context).size;

    final List<Widget> imageWidgets = List.generate(
      imagesTitle.length,
      (index) {
        return Container(
          width: (size.width - 30) / 4,
          child: ImgAndTextInColumn(
            heightImg: heightImg,
            widthImg: widthImg,
            heightSizedBox: heightsizedBox,
            ImgTitle: imagesTitle[index].toCapitalized(),
            ImgSrc: imagesSrc[imagesTitle[index]],
            heightText: heightText,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SanPhamShop(
                            index: index,
                            indexTitle: imagesTitle[index].toUpperCase(),
                            indexImg: imagesSrc[imagesTitle[index]],
                          )));
              //           // xử lý sự kiện khi ấn vào hình ảnh
            },
          ),
        );
      },
    );

    return BaseScreen(
      hideAppBar: true,
      body: Column(
        children: [
          Container(
            height: 180,
            width: size.width,
            child: CustomPaint(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Next Vision",
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              height: 25,
                              width: 95,
                              child: ElevatedButton(
                                  onPressed: () {},
                                  child: Text(
                                    "Số địa chỉ",
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                      primary:
                                          Color.fromARGB(255, 185, 179, 179))),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            SizedBox(
                              height: 25,
                              width: 95,
                              child: ElevatedButton(
                                  onPressed: () {},
                                  child: Text(
                                    "Đơn hàng",
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                      primary:
                                          Color.fromARGB(255, 185, 179, 179))),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Icon(Icons.search),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              child: Icon(Icons.shopping_cart_rounded),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              child: Icon(Icons.more_vert_rounded),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              painter: CurvePainterPage(),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: size.height - 250,
            width: size.width,
            child: Padding(
              padding: EdgeInsets.only(left: 15, right: 15, top: 15),
              child: ListView(
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: 180,
                        width: size.width - 5,
                        child: PageView.builder(
                          itemCount: images.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Image.asset(
                                  images[index],
                                  fit: BoxFit.cover,
                                ));
                          },
                          onPageChanged: (int index) {
                            setState(() {
                              _currentPage = index;
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(
                          images.length,
                          (int index) {
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  _currentPage = index;
                                });
                              },
                              child: Container(
                                width: 15.0,
                                height: 15.0,
                                margin: EdgeInsets.symmetric(horizontal: 4.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _currentPage == index
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  Wrap(spacing: 0, runSpacing: 30, children: imageWidgets),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// viet hoa

extension StringCasingExtension on String {
  String toCapitalized() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
