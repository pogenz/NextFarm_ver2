import 'package:flutter/material.dart';
import 'package:next_farm_ver2/res/dimens.dart';
import 'package:next_farm_ver2/ui/screen/homepage/homepage.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';

class GioHangShop extends StatefulWidget {
  const GioHangShop({super.key});

  @override
  State<GioHangShop> createState() => _GioHangShopState();
}

class _GioHangShopState extends State<GioHangShop> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      hideAppBar: true,
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: EdgeInsets.only(left: 5, right: 5),
              child: Container(
                height: 60,
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(color: Colors.grey, width: 2))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(Icons.arrow_back_ios)),
                        Text(
                          "Giỏ hàng".toCapitalized(),
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                    Container(
                      height: 40,
                      width: 40,
                      child: Icon(Icons.more_vert),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        color: Colors.grey[300],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
