import 'package:flutter/material.dart';

class ImgAndTextInColumn extends StatelessWidget {
  final VoidCallback? onPressed;
  final double? heightImg;
  final double? widthImg;
  final double? heightText;
  final double? heightSizedBox;
  final String? ImgSrc;
  final String? ImgTitle;
  const ImgAndTextInColumn({
    super.key,
    this.heightImg,
    this.heightText,
    this.widthImg,
    this.heightSizedBox,
    this.onPressed,
    this.ImgSrc,
    this.ImgTitle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: onPressed,
          child: Container(
            width: heightImg,
            height: widthImg,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage(ImgSrc ?? ""), fit: BoxFit.cover)),
          ),
        ),
        SizedBox(
          height: heightSizedBox ?? 0,
        ),
        Text(
          ImgTitle ?? "",
          style: TextStyle(
              fontSize: heightText ?? 0.toDouble(),
              fontWeight: FontWeight.bold),
          overflow: TextOverflow.ellipsis,
        ),
        SizedBox(
          height: heightSizedBox ?? 0 * 2,
        ),
      ],
    );
  }
}
