import 'package:flutter/material.dart';

// class vẽ hình sóng
class CurvePainterPage extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Color.fromARGB(255, 44, 170, 5);
    paint.style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(0, size.height * .50); // điểm bắt đầu vẽ
    path.quadraticBezierTo(
        0, size.height * .60, size.width * .1565, size.height * .60);
    path.lineTo(size.width * .85, size.height * .60); // điểm dưới cùng bên phải

    path.quadraticBezierTo(
        size.width,
        size.height * .60, // đường cong bên phải
        size.width,
        size.height * .70);

    path.lineTo(size.width, 0); // điểm trên cùng bên phải
    path.lineTo(0, 0); // điểm trên cùng bên trái
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
