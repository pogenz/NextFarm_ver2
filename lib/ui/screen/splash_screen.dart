import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:next_farm_ver2/res/resources.dart';
import 'package:next_farm_ver2/routes.dart';
import 'package:next_farm_ver2/ui/screen/auth/login_screen_new.dart';
import 'package:next_farm_ver2/ui/screen/screen.dart';
import 'package:next_farm_ver2/utils/shared_preference.dart';
import 'package:scale_size/scale_size.dart';

import 'homepage/homepage.dart';
import 'myappNextfarm/myappNextfarm.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SplashState();
  }
}

class _SplashState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) => openScreen(context));
  }

  Widget build(BuildContext context) {
    ScaleSize.init(context, designWidth: 375, designHeight: 812);
    return Scaffold(
      body: Container(
        color: AppColors.base_color,
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(AppImages.LOGIN_BACKGROUND),
                  fit: BoxFit.cover,
                ),
              ), /* add child content here */
            ),
          ],
        ),
      ),
    );
  }

  openScreen(BuildContext context) async {
    SharedPreferenceUtil.clearData();
    String token = await SharedPreferenceUtil.getToken();
    await Future.delayed(Duration(seconds: 2));
    if (token.isEmpty) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginNewScreen()));
      // Navigator.push(
      //     context, MaterialPageRoute(builder: (context) => MyAppNextFarm()));
    } else {
      Navigator.pushReplacementNamed(context, Routes.bottombar);
    }
  }
}
