import 'package:next_farm_ver2/blocs/base_bloc/base.dart';
import 'package:next_farm_ver2/blocs/cubit/login_cubit.dart';
import 'package:next_farm_ver2/res/resources.dart';
import 'package:next_farm_ver2/routes.dart';
import 'package:next_farm_ver2/ui/screen/auth/Forgot_pass_screen.dart';

import 'package:next_farm_ver2/ui/widget/widget.dart';
import 'package:next_farm_ver2/utils/shared_preference.dart';

// import 'package:next_farm_ver2/utils/shared_preference.dart';
import 'package:flutter/material.dart';
import 'package:next_farm_ver2/blocs/cubit/cubit.dart';

// import 'package:next_farm_ver2/ui/screen/screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginCubit>(
        create: (_) => LoginCubit(), child: LoginBody());
  }
}

class LoginBody extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginBody> {
  GlobalKey<TextFieldState> keyName = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> keyPass = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> keyCompany = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> keyRePass = GlobalKey<TextFieldState>();
  String language = "VN";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      loadingWidget: CustomLoading<LoginCubit>(),
      messageNotify: CustomSnackBar<LoginCubit>(),
      title: BaseButton(
        backgroundColor: Colors.transparent,
        title: "VN/${language == "VN" ? "EN" : "EN"}",
        textColor: (language == "EN") ? Colors.black26 : Colors.black,
        alignment: Alignment.topRight,
        width: 70,
        onTap: () {
          setState(() {
            language = (language == "VN") ? "EN" : "VN";
          });
        },
      ),
      hiddenIconBack: true,
      body: SingleChildScrollView(
        child: Container(
          color: AppColors.base_color_border_textfield,
          child: Column(
            children: [
              BlocListener<LoginCubit, BaseState>(
                listener: (_, state) {
                  if (state is LoadedState) {
                    //  // TODO @nambuidanh
                    // goto main
                  }
                },
                child: Container(
                  padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: Image.asset(
                    AppImages.LOGO,
                    fit: BoxFit.fill,
                    width: 150,
                    height: 150,
                  ),
                ),
              ),
              Container(
                child: CustomTextInput(
                  validator: (String company) {
                    if (company.isEmpty) {
                      return "Hãy nhập tên công ty!";
                    }
                    return "";
                  },
                  key: keyCompany,
                  enableBorder: false,
                  obscureText: false,
                  hideUnderline: true,
                  hintText: "Nhập tên công ty",
                ),
              ),
              SizedBox(
                height: 1,
              ),
              Container(
                child: CustomTextInput(
                  validator: (String username) {
                    if (username.isEmpty) {
                      return "Hãy nhập tên đăng nhập!";
                    }
                    return "";
                  },
                  key: keyName,
                  enableBorder: false,
                  obscureText: false,
                  hideUnderline: true,
                  hintText: "Nhập tên đăng nhập",
                ),
              ),
              SizedBox(
                height: 1.sw,
              ),
              Container(
                child: CustomTextInput(
                    validator: (String pass) {
                      if (pass.isEmpty) {
                        return "Hãy nhập pass!";
                      } else
                        return "";
                    },
                    key: keyPass,
                    enableBorder: false,
                    hideUnderline: true,
                    isPasswordTF: true,
                    suffixIcon: IconButton(
                      icon: Icon(Icons.visibility),
                      onPressed: () {},
                    ),
                    hintText: "Nhập mật khẩu"),
              ),
              SizedBox(
                height: 1.sw,
              ),
              Container(
                child: CustomTextInput(
                  validator: (String RePass) {
                    if (RePass.isEmpty) {
                      return "Hãy nhập lại pass";
                      // } else if (RePass != keyPass) {
                      //   return "Pass khong khớp";
                    } else if (RePass == keyPass){
                      return "";
                    }
                  },
                  key: keyRePass,
                  enableBorder: false,
                  hideUnderline: true,
                  isPasswordTF: true,
                  suffixIcon: IconButton(
                    icon: Icon(Icons.visibility),
                    onPressed: () {},
                  ),
                  hintText: "Nhập lại mật khẩu",
                ),
              ),
              SizedBox(
                height: 10.sw,
              ),
              BaseButton(
                onTap: () {
                  String username = keyName.currentState?.value ?? "";
                  String password = keyPass.currentState?.value ?? "";
                  String company = keyPass.currentState?.value ?? "";
                  String Repassword = keyPass.currentState?.value ?? "";
                  if (keyName.currentState!.isValid ||
                      keyPass.currentState!.isValid ||
                      keyCompany.currentState!.isValid ||
                      keyRePass.currentState!.isValid) {
                    // BlocProvider.of<LoginCubit>(context)
                    //     .login(username, password);
                    Navigator.of(context)
                        .pushReplacementNamed(Routes.bottombar);
                    SharedPreferenceUtil.saveToken('token');
                  }
                },
                margin:
                EdgeInsets.symmetric(horizontal: 30.sw, vertical: 10.sw),
                title: "Đăng ký",
                height: 45,
                width: 1000,
                textColor: Colors.green,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black26,
                          blurRadius: 15,
                          offset: Offset(1, 1))
                    ],
                    color: AppColors.base_color_border_textfield),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  BaseButton(
                    backgroundColor: Colors.transparent,
                    title: "Quên mật khẩu?",
                    textColor: Colors.blue,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Forgotpassscreen()));
                      // Navigator.of(context).pushNamed(Routes.forgotPassScreen);
                    },
                  ),
                  SizedBox(
                    width: 100.sw,
                  ),
                  BaseButton(
                    backgroundColor: Colors.transparent,
                    title: "Đăng nhập",
                    textColor: Colors.blue,
                    onTap: () {
                      Navigator.pushNamed(context, Routes.loginScreen);
                    },
                  ),
                ],
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Text('www.facebook.com/nextfarm.vn'),
                ),
              ),
            ],
          ),
          height: MediaQuery.of(context).size.height,
        ),
      ),
    );
  }
}
