import 'package:next_farm_ver2/blocs/base_bloc/base.dart';
import 'package:next_farm_ver2/blocs/cubit/login_cubit.dart';
import 'package:next_farm_ver2/res/resources.dart';
import 'package:next_farm_ver2/routes.dart';
import 'package:next_farm_ver2/ui/screen/auth/Forgot_pass_screen.dart';
import 'package:next_farm_ver2/ui/screen/auth/Sign_up_screen.dart';
import 'package:next_farm_ver2/ui/screen/cau_hinh_tuoi/cau_hinh_tuoi.dart';
import 'package:next_farm_ver2/ui/screen/homepage/homepage.dart';
import 'package:next_farm_ver2/ui/widget/widget.dart';
import 'package:next_farm_ver2/utils/shared_preference.dart';
// import 'package:next_farm_ver2/utils/shared_preference.dart';
import 'package:flutter/material.dart';
// import 'package:next_farm_ver2/ui/screen/screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';

import '../myappNextfarm/myappNextfarm.dart';

class LoginNewScreen extends StatelessWidget {
  const LoginNewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginCubit>(
        create: (_) => LoginCubit(), child: LoginBody());
  }
}

class LoginBody extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginBody> {
  GlobalKey<TextFieldState> keyName = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> keyPass = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> keyCompany = GlobalKey<TextFieldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      loadingWidget: CustomLoading<LoginCubit>(),
      messageNotify: CustomSnackBar<LoginCubit>(),
      title: Align(
        alignment: Alignment.bottomRight,
        child:
            Text("VN/EN", style: TextStyle(color: Colors.black, fontSize: 14)),
      ),
      hiddenIconBack: true,
      // customAppBar: TextButton(onPressed:  _toggleLanguage, child: Text(_isVietnamese ? 'Tiếng Việt' : 'English'),),
      body: ListView(
        children: [
          Container(
            color: AppColors.base_color_border_textfield,
            child: Column(
              children: [
                BlocListener<LoginCubit, BaseState>(
                  listener: (_, state) {
                    if (state is LoadedState) {}
                  },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
                    child: Image.asset(
                      AppImages.LOGO,
                      fit: BoxFit.fill,
                      width: 150,
                      height: 150,
                    ),
                  ),
                ),
                Container(
                  child: CustomTextInput(
                    validator: (String company) {
                      if (company.isEmpty) {
                        return "Hãy nhập tên công ty!";
                      }
                      return "";
                    },
                    key: keyCompany,
                    enableBorder: false,
                    obscureText: false,
                    hideUnderline: true,
                    hintText: "Nhập tên công ty",
                  ),
                ),
                SizedBox(
                  height: 1,
                ),
                Container(
                  child: CustomTextInput(
                    validator: (String username) {
                      if (username.isEmpty) {
                        return "Hãy nhập tên đăng nhập!";
                      }
                      return "";
                    },
                    key: keyName,
                    enableBorder: false,
                    obscureText: false,
                    hideUnderline: true,
                    hintText: "Nhập tên đăng nhập",
                  ),
                ),
                SizedBox(
                  height: 1.sw,
                ),
                Container(
                  child: CustomTextInput(
                      validator: (String pass) {
                        if (pass.isEmpty) {
                          return "Hãy nhập pass!";
                        } else
                          return "";
                      },
                      key: keyPass,
                      enableBorder: false,
                      hideUnderline: true,
                      // prefixIcon: Padding(
                      //   padding: EdgeInsets.all(8.sw),
                      //   child: Icon(
                      //     Icons.lock,
                      //   ),
                      // ),
                      isPasswordTF: true,
                      suffixIcon: IconButton(
                        icon: Icon(Icons.visibility),
                        onPressed: () {},
                      ),
                      hintText: "Nhập mật khẩu"),
                ),
                SizedBox(
                  height: 10.sw,
                ),
                BaseButton(
                  onTap: () {
                    // String username = keyName.currentState?.value ?? "";
                    // String password = keyPass.currentState?.value ?? "";
                    // String company = keyCompany.currentState?.value ?? "";
                    if (keyName.currentState!.isValid ||
                        keyPass.currentState!.isValid ||
                        keyCompany.currentState!.isValid) {
                      // BlocProvider.of<LoginCubit>(context)
                      //     .login(username, password);
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => MyAppNextFarm()));
                      SharedPreferenceUtil.saveToken('token');
                    }
                  },
                  margin:
                      EdgeInsets.symmetric(horizontal: 30.sw, vertical: 10.sw),
                  title: "Đăng nhập",
                  height: 45,
                  width: 1000,
                  textColor: Colors.green,
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black26,
                            blurRadius: 10,
                            blurStyle: BlurStyle.normal,
                            offset: Offset(1, 1))
                      ],
                      color: AppColors.base_color_border_textfield),
                ),
                SizedBox(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    BaseButton(
                      backgroundColor: Colors.transparent,
                      title: "Quên mật khẩu?",
                      textColor: Colors.blue,
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Forgotpassscreen()));
                        // Navigator.of(context)
                        //     .pushNamed(Routes.forgotPassScreen);
                      },
                    ),
                    SizedBox(
                      width: 100.sw,
                    ),
                    BaseButton(
                      backgroundColor: Colors.transparent,
                      title: "Đăng ký",
                      textColor: Colors.blue,
                      onTap: () {
                        // Navigator.pushNamed(context, Routes.registerScreen);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpScreen()));
                      },
                    ),
                  ],
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text('www.facebook.com/nextfarm.vn'),
                  ),
                ),
              ],
            ),
            height: MediaQuery.of(context).size.height,
          ),
        ],
      ),
    );
  }
}
