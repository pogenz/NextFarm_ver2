import 'package:flutter/material.dart';
import 'package:next_farm_ver2/ui/screen/Monitor_Screen/Monitor_Screen.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';
import 'package:next_farm_ver2/ui/screen/drawScreen/curvePainterpage.dart';
import 'package:next_farm_ver2/ui/screen/cau_hinh_tuoi/cau_hinh_tuoi.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // tham so
  int _selectedIndex = 0;
  DateTime now = new DateTime.now();
  int fontsize_7 = 7;
  int fontsize_8 = 8;
  int fontsize_9 = 9;
  int fontsize_10 = 10;
  int fontsize_15 = 15;
  int fontsize_20 = 20;
  int fontsize_25 = 25;
  int height_3 = 3;
  int height_5 = 5;
  int height_7 = 7;
  int height_10 = 10;
  int height_15 = 15;
  int height_20 = 20;
  int height_45 = 45;
  String trang_thai_thoi_tiet = "mưa to";
  String do_am = "56%";
  String nhiet_do = "27%";
  String dia_chi = "lam sơn , thanh hóa";

  // slide show img
  final List<String> images = [
    "assets/images/dong_lua1.jpeg",
    "assets/images/dong_lua2.jpeg",
  ];

  int _currentPage = 0;

  static List<Widget> _widgetOptions = <Widget>[
    Text("Home"),
    Text("Tin tuc"),
    Text("Shop"),
    Text("Tai khoan")
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  // lấy ngày tháng

  int getyear(DateTime now) {
    int year;
    return year = now.year;
  }

  int getmonth(DateTime now) {
    int month;
    return month = now.month;
  }

  int getday(DateTime now) {
    int day;
    return day = now.day;
  }

  // viết chữ đầu hoa

  String toUpperText(String textInput) {
    String textOutput = "";
    String first_textOutput = textInput[0].toUpperCase();
    for (int i = 0; i < textInput.length; i++) {
      if (i > 0) {
        textOutput = textOutput + textInput[i];
      }
    }
    textOutput = first_textOutput + textOutput;
    return textOutput;
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return BaseScreen(
      // backgroundcolor: Colors.white,
      // customAppBar: AppBar(
      //   title: Text("HomePage"),
      //   centerTitle: true,
      // ),
      hideAppBar: true,

      body: ListView(children: [
        Container(
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Container(
              child: Icon(
                Icons.notifications_none,
                size: 40,
              ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.only(left: 20),
                child: Image(
                  image: AssetImage("assets/images/nextfarm.png"),
                  height: 100,
                ),
              ),
            ),
            Container(
              child: Image(
                image: AssetImage("assets/images/dientrach.png"),
                height: 70,
              ),
            ),
          ]),
        ),
        SizedBox(
          height: height_15.toDouble(),
        ),
        Container(
          height: 250,
          width: size.width,
          child: CustomPaint(
            painter: CurvePainterPage(),
            child: Padding(
              padding: EdgeInsets.only(
                left: 15,
                top: 15,
                right: 15,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "Ngày ${getday(now)} tháng ${getmonth(now)} năm ${getyear(now)}",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: fontsize_20.toDouble()),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  SizedBox(
                    height: height_7.toDouble(),
                  ),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromARGB(255, 75, 74, 74),
                              spreadRadius: 2,
                              blurRadius: 5,
                              offset: Offset(0, 3)),
                        ]),
                    child: Padding(
                      padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                      child: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Tình hình thời tiết",
                                style: TextStyle(
                                    fontSize: fontsize_10.toDouble(),
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700),
                              ),
                              Text(
                                "${toUpperText(trang_thai_thoi_tiet)}",
                                style: TextStyle(
                                    color: Colors.red[700],
                                    fontSize: fontsize_15.toDouble(),
                                    fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                          Spacer(),
                          Container(
                            width: size.width * .60,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Column(
                                  children: [
                                    Image(
                                      image:
                                          AssetImage("assets/images/do_am.jpg"),
                                      height: height_20.toDouble(),
                                    ),
                                    Text(
                                      toUpperText("độ ẩm"),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: fontsize_7.toDouble(),
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Text(
                                      "${do_am}",
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: fontsize_10.toDouble(),
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                                Spacer(),
                                Column(
                                  children: [
                                    Image(
                                      image: AssetImage(
                                          "assets/images/nhiet_do.jpg"),
                                      height: height_20.toDouble(),
                                    ),
                                    Text(
                                      toUpperText("nhiệt độ"),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: fontsize_7.toDouble(),
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Text(
                                      "${nhiet_do}",
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: fontsize_10.toDouble(),
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                                Spacer(),
                                Column(
                                  children: [
                                    Container(
                                      width: size.width * .30,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Image(
                                            image: AssetImage(
                                                "assets/images/mua.jpg"),
                                            height: height_20.toDouble(),
                                          ),
                                          Text(
                                            "${nhiet_do}",
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontWeight: FontWeight.bold,
                                                fontSize:
                                                    fontsize_25.toDouble()),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Text(
                                      "${dia_chi}".toTitleCase(),
                                      style: TextStyle(
                                          fontSize: fontsize_9.toDouble(),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 130,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromARGB(255, 75, 74, 74),
                              spreadRadius: 2,
                              blurRadius: 3,
                              offset: Offset(0, 3)),
                        ]),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: height_45.toDouble(),
                              height: height_45.toDouble(),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/giam_sat.jpg"),
                                      fit: BoxFit.cover)),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "giám sát".toCapitalized(),
                              style: TextStyle(
                                  fontSize: fontsize_15.toDouble(),
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MonitorScreen()));
                              },
                              child: Container(
                                width: height_45.toDouble(),
                                height: height_45.toDouble(),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/bao_cao.jpg"),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "báo cáo".toCapitalized(),
                              style: TextStyle(
                                  fontSize: fontsize_15.toDouble(),
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            WateringScreen()));
                              },
                              child: Container(
                                width: height_45.toDouble(),
                                height: height_45.toDouble(),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/cau_hinh_tuoi.jpg"),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "cấu hình tưới".toCapitalized(),
                              style: TextStyle(
                                  fontSize: fontsize_15.toDouble(),
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: height_45.toDouble(),
                              height: height_45.toDouble(),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/vi_khi_hau.png"),
                                      fit: BoxFit.cover)),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "vi khí hậu".toCapitalized(),
                              style: TextStyle(
                                  fontSize: fontsize_15.toDouble(),
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          height: 220,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(
                    color: Color.fromARGB(255, 121, 177, 58), width: 2)),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 15, right: 15, top: 3),
            child: Column(
              children: [
                Expanded(
                  child: PageView.builder(
                    itemCount: images.length,
                    itemBuilder: (BuildContext context, int index) {
                      // return Image.asset(
                      //   images[index],
                      //   fit: BoxFit.cover,
                      // );
                      return Stack(
                        children: [
                          Image.asset(
                            images[index],
                            width: size.width - 30,
                            fit: BoxFit.cover,
                          ),
                          Positioned(
                              top: 5,
                              right: 5,
                              child: Image.asset(
                                "assets/images/nextfarm.png",
                                height: height_45.toDouble(),
                                fit: BoxFit.cover,
                              ))
                        ],
                      );
                    },
                    onPageChanged: (int index) {
                      setState(() {
                        _currentPage = index;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: height_10.toDouble(),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                    images.length,
                    (int index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            _currentPage = index;
                          });
                        },
                        child: Container(
                          width: 15.0,
                          height: 15.0,
                          margin: EdgeInsets.symmetric(horizontal: 4.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _currentPage == index
                                ? Colors.blue
                                : Colors.grey,
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tin hot".toUpperCase(),
                      style: TextStyle(
                          fontSize: fontsize_20.toDouble(),
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        "xem thêm".toUpperCase(),
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: fontsize_15.toDouble(),
                            fontWeight: FontWeight.w300),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: height_5.toDouble(),
                )
              ],
            ),
          ),
        ),
        Container(
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.only(right: 15, left: 15, top: 10),
            child: Image.asset(
              "assets/images/rung_cay2.jpeg",
              fit: BoxFit.cover,
            ),
          ),
        ),
      ]),
      customBottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.amber),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.document_scanner),
            label: 'Tin tuc',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart_sharp),
            label: 'Shop',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Tai khoan',
          ),
        ],
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.orange[700],
        unselectedItemColor: Colors.grey,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        onTap: _onItemTapped,
      ),
    );
  }
}

// viet hoa

extension StringCasingExtension on String {
  String toCapitalized() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
