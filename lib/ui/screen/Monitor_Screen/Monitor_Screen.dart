import 'package:flutter/material.dart';
import 'package:next_farm_ver2/ui/screen/auth/login_screen_new.dart';
import 'package:next_farm_ver2/ui/screen/homepage/homepage.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';

class MonitorScreen extends StatefulWidget {
  const MonitorScreen({Key? key}) : super(key: key);

  @override
  State<MonitorScreen> createState() => _MonitorScreenState();
}

class _MonitorScreenState extends State<MonitorScreen> {
  List<String> listtubleft = <String>[
    'Bồn 1:',
    'Bồn 2:',
    'Bồn 3:',
    'Bồn 4:',
    'Bồn 5:',
    'Bồn 6:',
  ];

  List<String> listtubright = <String>[
    'Bồn 7:',
    'Bồn 8:',
    'Bồn 9:',
    'Bồn 10:',
    'Bồn 11:',
    'Bồn 12:',
  ];

  bool _switchValuelan = false;
  bool _switchValuehoa = false;
  bool _switchValuecanh = false;
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      customAppBar: AppBar(
        titleSpacing: 10,
        elevation: 0,
        backgroundColor: Colors.green,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            icon: Icon(Icons.home),
            color: Colors.white,
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomePage()));
            },
          ),
        ],
      ),
      backgroundcolor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: SizedBox(
          child: ListView(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Center(
                  child: Text(
                    "Giám sát hệ thống tưới",
                    style: TextStyle(fontSize: 30, color: Colors.black),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Table(
                border: TableBorder.all(color: Colors.green),
                children: [
                  TableRow(children: [
                    TableCell(
                      child: Container(
                        padding: EdgeInsets.all(4),
                        child: Text("Khu vực tưới:",
                            style: TextStyle(fontSize: 18), textAlign: TextAlign.left),
                      ),
                    ),
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Container(
                        padding: EdgeInsets.all(4),
                        child:
                        Text("Cây trồng:", style: TextStyle(fontSize: 18), textAlign: TextAlign.left),
                      ),
                    ),
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Container(
                        padding: EdgeInsets.all(4),
                        child: Text("Giai đoạn tưới:",
                            style: TextStyle(fontSize: 18), textAlign: TextAlign.left),
                      ),
                    ),
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Container(
                        padding: EdgeInsets.all(4),
                        child: Text("Thời gian tưới:",
                            style: TextStyle(fontSize: 18), textAlign: TextAlign.left),
                      ),
                    ),
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Container(
                        padding: EdgeInsets.all(4),
                        child: Text("Lượng nước đã tưới:",
                            style: TextStyle(fontSize: 18), textAlign: TextAlign.left),
                      ),
                    ),
                  ]),
                ],
              ),
              Table(
                  border: TableBorder.all(
                      color: Colors.green,
                      width: 1
                  ),
                  children: [
                    TableRow(children: [
                      TableCell(
                        child: Column(
                          children: [
                            ListView.builder(
                              padding: EdgeInsets.all(4),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 6,
                              itemBuilder: (context, index) {
                                return Text(listtubleft[index],
                                    style: TextStyle(fontSize: 18));
                              },
                            ),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: [
                            ListView.builder(
                              padding: EdgeInsets.all(4),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 6,
                              itemBuilder: (context, index) {
                                return Text(listtubright[index],
                                    style: TextStyle(fontSize: 18));
                              },
                            ),
                          ],
                        ),
                      ),
                    ])
                  ]),
              SizedBox(height: 10),
              Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: Column(
                          children: [
                            Text("ĐỘ ẨM KHÔNG KHÍ", style: TextStyle( fontSize: 12)),
                            Text("None %", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Text("ÁNH SÁNG", style: TextStyle( fontSize: 12)),
                          Text("None lux", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)
                        ],
                      ),
                    ),

                    SizedBox(width: 9),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.lightGreen,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        padding: EdgeInsets.fromLTRB(8, 10, 8, 10),
                        child: Column(
                          children: [
                            Text("NHIỆT ĐỘ KHÔNG KHÍ", style: TextStyle( fontSize: 12)),
                            Text("None oC", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.blueGrey,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Column(
                    children: [
                      Text("EC", style: TextStyle( fontSize: 12)),
                      Text("None mS/cm", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 300,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.black38,
                            width: 1,
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          Text("Quạt vườn lan", textAlign: TextAlign.left),
                          Spacer(),
                          Icon(Icons.access_alarms_outlined),
                          Switch(

                            activeColor: Colors.green,
                            value: _switchValuelan,
                            onChanged: (value) {
                              setState(() {
                                _switchValuelan = value;
                              });
                            },
                          )
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.black38,
                            width: 0.8,
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          Text("Quạt vườn hoa", textAlign: TextAlign.left),
                          Spacer(),
                          Icon(Icons.access_alarms_outlined),
                          Switch(
                            activeColor: Colors.green,
                            value: _switchValuehoa,
                            onChanged: (value) {
                              setState(() {
                                _switchValuehoa = value;
                              });
                            },
                          )
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.black38,
                            width: 0.8,
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          Text("Bơm thuỷ canh", textAlign: TextAlign.left),
                          Spacer(),
                          Icon(Icons.access_alarms_outlined),
                          Switch(
                            activeColor: Colors.green,
                            value: _switchValuecanh,
                            onChanged: (value) {
                              setState(() {
                                _switchValuecanh = value;
                              });
                            },
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
