import 'package:flutter/material.dart';
import 'package:next_farm_ver2/ui/widget/base_screen.dart';
import 'package:next_farm_ver2/data/model/user_model.dart';

import '../user/user.dart';

class TaiKhoan extends StatefulWidget {
  const TaiKhoan({super.key});

  @override
  State<TaiKhoan> createState() => _TaiKhoanState();
}

class _TaiKhoanState extends State<TaiKhoan> {
  List<UserModel> listUser = [
    UserModel(
        email: "email1",
        id: 1,
        username: "username1",
        userImg: "assets/images/1.jpeg"),
    UserModel(
        email: "email2",
        id: 2,
        username: "username2",
        userImg: "assets/images/2.jpeg"),
    UserModel(
        email: "email3",
        id: 3,
        username: "username3",
        userImg: "assets/images/3.jpeg"),
    UserModel(
        email: "email4",
        id: 4,
        username: "username4",
        userImg: "assets/images/4.jpeg"),
    UserModel(
        email: "email5",
        id: 5,
        username: "username5",
        userImg: "assets/images/5.jpeg"),
    UserModel(
        email: "email6",
        id: 6,
        username: "username6",
        userImg: "assets/images/6.jpeg"),
    UserModel(
        email: "email7",
        id: 7,
        username: "username7",
        userImg: "assets/images/7.jpeg"),
    UserModel(
        email: "email8",
        id: 8,
        username: "username8",
        userImg: "assets/images/8.jpeg"),
    UserModel(
        email: "email9",
        id: 9,
        username: "username9",
        userImg: "assets/images/9.jpeg"),
    UserModel(
        email: "email10",
        id: 10,
        username: "username10",
        userImg: "assets/images/10.jpeg"),
  ];
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return BaseScreen(
      hideAppBar: true,
      body: Container(
        height: size.height,
        width: size.width,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.only(left: 5, right: 5, top: 40),
          child: Container(
            height: size.height - 50,
            width: size.width,
            child: ListView.builder(
              itemCount: listUser.length,
              itemBuilder: ((context, index) => InkWell(
                    // child: Container(
                    //   child: Text(listUser[index].email ?? ""),
                    // ),
                    child: Container(
                      child: Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Row(
                          children: [
                            Container(
                              height: 80,
                              width: 80,
                              // decoration: BoxDecoration(
                              //     borderRadius:
                              //         BorderRadius.all(Radius.circular(50))),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(30),
                                child: Image(
                                  image:
                                      AssetImage(listUser[index].userImg ?? ""),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Column(
                              children: [
                                Text(
                                  "${listUser[index].username ?? ""}"
                                      .toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w300,
                                      color: Colors.pink),
                                ),
                                Text(
                                  "${listUser[index].email ?? ""}"
                                      .toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w300),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => User(listUser[index])),
                      );
                    },
                  )),
            ),
          ),
        ),
      ),
    );
  }
}
